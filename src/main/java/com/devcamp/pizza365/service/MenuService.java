package com.devcamp.pizza365.service;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CMenu;

@Service
public class MenuService {
    CMenu comboSmall = new CMenu("small","20cm","2","200gr",2,150000);
    CMenu comboMedium = new CMenu("medium","25cm","4","300gr",3,200000);
    CMenu comboLarge = new CMenu("large","30cm","8","500gr",4,250000);
    public ArrayList<CMenu> getAllMenu(){
        ArrayList<CMenu> listMenu = new ArrayList<>();
        listMenu.add(comboSmall);
        listMenu.add(comboMedium);
        listMenu.add(comboLarge);
        return listMenu;
    }

}
