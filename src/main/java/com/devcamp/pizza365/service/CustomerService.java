package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.Customer;
import com.devcamp.pizza365.model.Order;
import com.devcamp.pizza365.repository.ICustomerRepository;

@Service
public class CustomerService {
    @Autowired
    ICustomerRepository customerRepository;
    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customerList = new ArrayList<>();
        customerRepository.findAll().forEach(customerList:: add);
        return customerList;
    }
    public Set<Order> getOrderByCustomerId(long customerId){
        Customer vCustomer = customerRepository.findByCustomerId(customerId);
        if ( vCustomer != null){
            return vCustomer.getOrders();
        }
        else return null;
    }

}
