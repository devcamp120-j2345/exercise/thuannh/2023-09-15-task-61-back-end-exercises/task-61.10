package com.devcamp.pizza365.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.Drink;
import com.devcamp.pizza365.service.DrinkService;

@RestController
@CrossOrigin
public class DrinkController {
    @Autowired
    DrinkService drinkService;
    @GetMapping("/drinks")
    public ResponseEntity<List<Drink>> getDrinks(){
        try {
            return new ResponseEntity<>(drinkService.getAllDrinks(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
